package com.domain.test;

import io.sentry.Sentry;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestApplication {
	public static void main(String[] args) {
//		Sentry.capture("Application started");
		SpringApplication.run(TestApplication.class, args);
	}
}
