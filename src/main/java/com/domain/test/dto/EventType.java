package com.domain.test.dto;

public enum EventType {
    CREATE, UPDATE, REMOVE
}
