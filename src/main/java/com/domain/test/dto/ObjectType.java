package com.domain.test.dto;

public enum ObjectType {
    MESSAGE, COMMENT
}
