import Vue from 'vue'
import Vuetify from 'vuetify'
import '@babel/polyfill'
import './api/resource.js'
import router from './router/router.js'
import App from './pages/App.vue'
import store from './store/store.js'
import { connect } from './util/ws.js'
import 'vuetify/dist/vuetify.min.css'
import * as Sentry from '@sentry/browser'
import * as Integrations from '@sentry/integrations'

Sentry.init({
    dsn: 'https://ffa620b1ee424a6b9342a96f71c9f96b@sentry.io/1451703',
    integrations: [
        new Integrations.Vue({
            Vue,
            attachProps: true,
        }),
    ],
})

Sentry.configureScope( scope =>
      scope.setUser({
          id: profile && profile.id,
          username: profile && profile.name
      })
)

if (profile) {
    connect()
}

Vue.use(Vuetify)

new Vue({
    el: '#app',
    store,
    router,
    render: a => a(App)
})

